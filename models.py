from django.db import models

class Cliente (models.Model):
    dni = models.ImageField(max_length=9,default="-")
    nombre = models.CharField(max_length=20)
    direccion = models.CharField(max_length=20)
    fecha_de_nacimiento = models.DateField()
    